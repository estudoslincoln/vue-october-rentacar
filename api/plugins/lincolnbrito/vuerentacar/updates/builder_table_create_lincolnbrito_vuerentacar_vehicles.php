<?php namespace LincolnBrito\Vuerentacar\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLincolnbritoVuerentacarVehicles extends Migration
{
    public function up()
    {
        Schema::create('lincolnbrito_vuerentacar_vehicles', function($table)
        {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->text('description')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('lincolnbrito_vuerentacar_vehicles');
    }
}
