<?php namespace LincolnBrito\Vuerentacar\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLincolnbritoVuerentacarVehicles2 extends Migration
{
    public function up()
    {
        Schema::table('lincolnbrito_vuerentacar_vehicles', function($table)
        {
            $table->boolean('available')->after('price');
        });
    }
    
    public function down()
    {
        Schema::table('lincolnbrito_vuerentacar_vehicles', function($table)
        {
            $table->dropColumn('available');
        });
    }
}
