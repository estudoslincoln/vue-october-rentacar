<?php namespace LincolnBrito\Vuerentacar\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLincolnbritoVuerentacarLocations extends Migration
{
    public function up()
    {
        Schema::create('lincolnbrito_vuerentacar_locations', function($table)
        {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->string('title');
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('lincolnbrito_vuerentacar_locations');
    }
}
