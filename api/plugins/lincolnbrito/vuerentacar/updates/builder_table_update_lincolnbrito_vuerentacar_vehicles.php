<?php namespace LincolnBrito\Vuerentacar\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLincolnbritoVuerentacarVehicles extends Migration
{
    public function up()
    {
        Schema::table('lincolnbrito_vuerentacar_vehicles', function($table)
        {
            $table->decimal('price', 10, 0)->nullable()->after('description');
        });
    }
    
    public function down()
    {
        Schema::table('lincolnbrito_vuerentacar_vehicles', function($table)
        {
            $table->dropColumn('price');
        });
    }
}
