<?php

use LincolnBrito\Vuerentacar\Models\Location;
use LincolnBrito\Vuerentacar\Models\Vehicle;

Route::get('vehicles', function(){
    $vehicles = Vehicle::with('locations')->get();

    return $vehicles;
});


Route::get('vehicles/filter/{id}', function($id){
    $vehicles = Vehicle::whereHas('locations', function($query) use ($id){
        $query->where('id', '=', $id);
    })->get();

    return $vehicles;
});

Route::get('locations', function(){
    $locations = Location::all();

    return $locations;
});

Route::get('locations/list', function(){

    $locations = Location::select('title as label','id as value')->get();

    return $locations;
});
