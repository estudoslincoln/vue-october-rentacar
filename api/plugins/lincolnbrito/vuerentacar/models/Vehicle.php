<?php namespace LincolnBrito\Vuerentacar\Models;

use Model;

/**
 * Model
 */
class Vehicle extends Model
{
    use \October\Rain\Database\Traits\Validation;

    protected $with = ['image'];

    /** Relations */
    public $belongsToMany = [
        'locations' => [
            'LincolnBrito\Vuerentacar\Models\Location',
            'table' => 'lincolnbrito_vuerentacar_vehicles_locations',
            'order' => 'title'
        ]
    ];

    public $attachOne = [
        'image' => 'System\Models\File'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'lincolnbrito_vuerentacar_vehicles';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
